from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# #Static Route
# @app.route('/')
# def index():
# 	return 'This is the Homepage "/"'

# @app.route('/Jane')
# def greet():
# 	return 'Good day, Jane! How are you?'

# #Dynamic Routes
# @app.route('/hello/<name>')
# def hello(name):
# 	return f'Hello, {name}'


# @app.route('/post/<int:post_id>')
# def show_post(post_id):
#     # show the post with the given id, the id is an integer
#     return f'Post {post_id}'


# Grocery List App


#This line of code creates a list named groceryList that contains a single item, "Chicken". This list will be used to store the grocery items entered by the user.
groceryList = ["Chicken"]


@app.route("/", methods=["GET"])
def index():
    return render_template("index.html", groceryList=groceryList, enumerate=enumerate)

@app.route("/add_groceryItem", methods=["POST"])
def add_groceryItem():
	groceryItem = request.form["item"]
	groceryList.append(groceryItem)
	return redirect(url_for("index"))

@app.route("/delete_groceryItem/<int:item_index>", methods=["POST"])
def delete_groceryItem(item_index):
    groceryList.pop(item_index)
    return redirect(url_for("index"))



if __name__ == "__main__":
	app.run(debug=True)
